from azure.storage.blob import BlockBlobService, ContentSettings
from config import constant

# def image_upload(image_name, formatted_image_name, color):
#     image_path = constant.SAN_LOCATION + '/' + color['color_variant_id'] + '/' + image_name
#     result = cloudinary.uploader.upload(image_path,
#                                         public_id=formatted_image_name)
#     return result

block_blob_service = ''


def _create_blob_obj():
    global block_blob_service
    if block_blob_service == '':
        block_blob_service = BlockBlobService(account_name=constant.AZURE_CONFIG['account_name'],
                                              account_key=constant.AZURE_CONFIG['api_secret_key'])


def image_upload(image_name, formatted_image_name, concept):
    _create_blob_obj()
    container_region = constant.APP_CONFIG['region'].lower() + '-media'
    if constant.APP_CONFIG['env'].upper() == 'UAT':
        container_region += '-' + constant.APP_CONFIG['env'].lower()

    block_blob_service.create_blob_from_path(
        container_region + '/' + concept,
        formatted_image_name,
        image_name,
        content_settings=ContentSettings(content_type='image/jpg')
    )
