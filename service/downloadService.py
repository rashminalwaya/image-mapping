from service import productService
from config import constant, database
import urllib, datetime, os
from PIL import Image
from os import path
from util import utility

utility_obj = utility.Utility()

global errors
errors = []


def upsert_tracker_document(product, status, db_name):
    tracker_doc = product
    if 'error' in tracker_doc and len(tracker_doc['error']) > 0:
        tracker_doc['download_status'] = 'failure'
    else:
        tracker_doc['download_status'] = status

    if tracker_doc['download_status'] == 'processed':
        tracker_doc['upload_status'] = 'pending'
    tracker_doc['updated_at'] = datetime.datetime.now()
    return database.db[db_name].find_and_modify({'base_product_id': product['base_product_id']},
                                               {"$set": tracker_doc})


def generate_final_image_list(product, color, delta):
    final_images = {}
    for image in color['images']:
        if image['delta'] in delta:
            data = {'dimension': 0, 'url': ''}
            if image['delta'] not in final_images:
                final_images[image['delta']] = data
            dimension = int(image['style'].split('WX')[0])
            if dimension > final_images[image['delta']]['dimension']:
                new_image_dict = {'dimension': dimension, 'url': image['url']}
                final_images[image['delta']] = new_image_dict
        else:
            data = {'dimension': 0, 'url': ''}
            if image['delta'] not in final_images:
                final_images[image['delta']] = data

            dimension = int(image['style'].split('WX')[0])

            if dimension > final_images[image['delta']]['dimension']:
                new_image_dict = {'dimension': dimension, 'url': image['url']}
                final_images[image['delta']] = new_image_dict
    return final_images


def generate_mapping_images(product, final_images, color, identifier):
    temp_dict = []
    size = 2100, 2100
    # download image from rack-space
    i = 0
    for f_key, f_value in final_images.iteritems():
        image_content = urllib.urlopen(f_value['url'])
        if image_content.getcode() == 200:
            tmp_data = {}
            f_image = (f_value['url'].split('/')[-1]).split('.')[0]

            directory_path = constant.DOWNLOAD_LOCATION + '/' + identifier
            if not os.path.exists(directory_path):
                os.makedirs(directory_path)

            file_name = identifier + '_' + utility_obj.delta_formation(
                f_key) + '.jpg'
            destination_path = directory_path + '/' + file_name

            if not path.exists(constant.SAN_LOCATION + '/' + f_image + '.jpg'):
                testfile = urllib.URLopener()
                testfile.retrieve(f_value['url'], destination_path)
                im = Image.open(destination_path)
                im_resized = im.resize(size, Image.ANTIALIAS)
                im_resized.save(destination_path, "JPEG")
            tmp_data['delta'] = f_key
            tmp_data['filename'] = str(file_name)
            tmp_data['rel_uri'] = str(identifier + '/' + file_name)
            temp_dict.append(tmp_data)
            i += 1
        else:
            color_error = {}
            color_error['delta'] = f_key
            color_error['errorCode'] = image_content.getcode()
            color_error['error'] = 'Image is missing at rackspace'

            if product['product_type'] != 'SINGLE-SKU':
                color_error['identifier'] = color['color_variant_id']
            else:
                color_error['identifier'] = product['base_product_id']

            errors.append(color_error)

    return temp_dict


def download_product_image(product, delta):
    # image not present at SAN location
    # call get product details
    final_images = {}
    product_detail = productService._get_product_detail(product)

    if product_detail is not None:
        image_error = {}
        if len(product_detail['images']) > 0:
            final_images = generate_final_image_list(product, product_detail, delta)
        else:

            image_error['identifier'] = product_detail['base_product_id']
            image_error['errorCode'] = '404'
            image_error['error'] = 'Images does not exit'
        if len(image_error) > 0:
            errors.append(image_error)

    image_response = generate_mapping_images(product, final_images, product_detail, product['base_product_id'])

    return image_response


def download_color_image(product, mapping_color, delta):
    # image not present at SAN location
    # call get product details
    final_images = {}
    product_detail = productService._get_product_detail(product)
    if product_detail is not None:
        for color in product_detail['colors']:
            if 'color_variant_id' in mapping_color:
                if color['color_variant_id'] == mapping_color['color_variant_id']:
                    image_error = {}
                    if len(color['images']) > 0:
                        final_images = generate_final_image_list(product, color, delta)
                    else:
                        image_error['identifier'] = color['color_variant_id']
                        image_error['errorCode'] = '404'
                        image_error['error'] = 'Images does not exit'

                    if len(image_error) > 0:
                        errors.append(image_error)

    image_response = generate_mapping_images(product, final_images, mapping_color, mapping_color['color_variant_id'])
    return [errors, image_response]
