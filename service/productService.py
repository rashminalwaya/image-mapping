from config import constant
import requests, json

ps_token = ''


def _get_token():
    global ps_token
    if ps_token == '':
        url = constant.PS_DOMAIN + '/api/v1/get-token'
        data = {'user_name': constant.PS_USERNAME, 'password': constant.PS_PASSWORD}
        res = requests.post(url, data=data)

        response = json.loads(res.content)
        ps_token = response['token']
    return ps_token


def _get_product_detail(product):
    _get_token()
    get_product_details_api = constant.PS_DOMAIN + '/api/v1/product/' + product['base_product_id']
    headers = {
        'Content-type': 'application/json',
        'Authorization': "Bearer" + ps_token,
        'Accept': 'application/json'
    }
    product_response = requests.get(get_product_details_api, headers=headers)
    return json.loads(product_response.content)
