from config import database, constant
from os import path
import sys
from service import downloadService
import csv

db_name = 'cc2_image_mappings'
concept = sys.argv[1]
category = sys.argv[2].split(',')
if len(category) > 0 and category[0] == '':
    category = database.db.cc2_image_mappings.distinct('categories.name', {'concept': concept})

product_ids = []
if len(sys.argv) > 3:
    product_list = sys.argv[3]

    with open('storage/' + product_list, 'rb') as csvfile:
        reader = csv.reader(csvfile)
        reader.next()
        for row in reader:
            product_ids.append(row[0])

# import pdb;pdb.set_trace()

condition_array = [
    {'$and': [{'concept': concept}, {'download_status': {'$exists': False}}, {'categories.name': {'$in': category}}]},
    {'$or': [{'colors.post_qc_desk_status': {'$in': ['done', 'finished']}},
             {'post_qc_desk_status': {'$in': ['done', 'finished']}}]}
]

if len(product_ids) > 0:
    condition_array = [

        {'$and': [{'concept': concept}, {'download_status': {'$exists': False}}, {'categories.name': {'$in': category}},
                  {'base_product_id': {'$in': product_ids}}]},
        {'$or': [{'colors.post_qc_desk_status': {'$in': ['done', 'finished']}},
                 {'post_qc_desk_status': {'$in': ['done', 'finished']}}]}
    ]

products = database.db.cc2_image_mappings.find(
    {
        '$and': condition_array
    }).limit(2)

for product in products:
    try:
        global error_bunch
        error_bunch = []
        tracker_detail = downloadService.upsert_tracker_document(product, 'pending',db_name)

        if product is not None:
            tracker_detail = downloadService.upsert_tracker_document(product, 'processing',db_name)
            if product['product_type'] != 'SINGLE-SKU':
                if len(product['colors']) > 0:
                    for mp_color_key, mp_color_value in enumerate(product['colors']):
                        if mp_color_value['post_qc_desk_status'] == 'done' or mp_color_value[
                            'post_qc_desk_status'] == 'finished':
                            delta = []
                            if len(mp_color_value['retouched_images']) == 0:
                                # call product service to get details
                                download_response = downloadService.download_color_image(
                                    product,
                                    mp_color_value,
                                    delta)
                                if len(download_response[1]) > 0:
                                    product['colors'][mp_color_key]['download_images'] = download_response[1]
                                if len(download_response[0]) > 0:
                                    error_bunch = download_response[0]
                            else:
                                # check the image at SAN location
                                for mpc_image_key, mpc_image_value in enumerate(mp_color_value['retouched_images']):

                                    image_name = mpc_image_value['url'].split('//')[-1].split('studioimages')[-1]
                                    if not path.exists(constant.SAN_LOCATION + image_name):
                                        delta.append(mpc_image_value['delta'])
                                if len(delta) > 0:
                                    download_response = downloadService.download_color_image(
                                        product,
                                        mp_color_value,
                                        delta)
                                    if len(download_response[1]) > 0:
                                        product['colors'][mp_color_key]['download_images'] = download_response[1]
                                    if len(download_response[0]) > 0:
                                        error_bunch = download_response[0]
            else:
                delta = []
                if product['post_qc_desk_status'] == 'done' or product['post_qc_desk_status'] == 'finished':
                    if len(product['retouched_images']) == 0:
                        download_images = downloadService.download_product_image(product, delta)
                        if len(download_images) > 0:
                            product['download_images'] = download_images
                    else:
                        # check the image at SAN location
                        for mp_image_key, mp_image_value in enumerate(product['retouched_images']):
                            image_name = mp_image_value['url'].split('//')[-1].split('studioimages')[-1]
                            if not path.exists(constant.SAN_LOCATION + image_name):
                                delta.append(mp_image_value['delta'])

                        if len(delta) > 0:
                            product['download_images'] = downloadService.download_product_image(
                                product, delta)
            if len(error_bunch) > 0:
                product['error'] = error_bunch

            downloadService.upsert_tracker_document(product, 'processed',db_name)
    except Exception, e:
        error = dict()
        error[product['base_product_id']] = 'Some exception occur while running download script!' + str(e)
        product['error'] = error
        downloadService.upsert_tracker_document(product, 'failure',db_name)
