from dotenv import load_dotenv
import os

load_dotenv()


def get_env(key, default=''):
    value = os.getenv(key)

    if not value and default != '':
        value = default
    return value


# constant.py
DATABASE_CONFIG = {
    'host': get_env('MONGODB_HOST'),
    'database': get_env('MONGODB_DATABASE'),
    'username': get_env('MONGODB_USERNAME'),
    'password': get_env('MONGODB_PASSWORD'),
    'port': get_env('MONGODB_PORT')
}
SAN_LOCATION = get_env('SAN_LOCATION')
CC1_SAN_LOCATION = get_env('CC1_SAN_LOCATION')

DOWNLOAD_LOCATION = get_env('DOWNLOAD_LOCATION')
CC1_DOWNLOAD_LOCATION = get_env('CC1_DOWNLOAD_LOCATION')

AZURE_CONFIG = {
    'domain': get_env('AZURE_DOMAIN'),
    'account_name': get_env('AZURE_ACCOUNT_NAME'),
    'api_secret_key': get_env('AZURE_SECRET_KEY')
}

CONCEPT = get_env('CONCEPT')
PS_DOMAIN = get_env('PS_DOMAIN')
PS_USERNAME = get_env('PS_USERNAME')
PS_PASSWORD = get_env('PS_PASSWORD')

APP_CONFIG = {
    'env': get_env('APP_ENV'),
    'region': get_env('APP_REGION')
}
