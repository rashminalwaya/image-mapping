from pymongo import MongoClient
import constant

host = constant.DATABASE_CONFIG['host']
port = constant.DATABASE_CONFIG['port']
hostString = host + ':' + port

# The following needs to be updated before running the script.
database = constant.DATABASE_CONFIG['database']
useAuthentication = False
username = constant.DATABASE_CONFIG['username']
password = constant.DATABASE_CONFIG['password']

# If username and password has been provided in .env file then useAuthentication
if username and password:
    useAuthentication = True

if useAuthentication:
    client = MongoClient("mongodb://" + username + ":" + password + "@" + host)
else:
    client = MongoClient(hostString)

# Connection to MongoDB database
db = client[database]
