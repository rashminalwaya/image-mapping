from config import database, constant
import sys, csv, datetime
from os import path
from service import azureService
from util import utility

utility_obj = utility.Utility()

concept = sys.argv[1]
category = sys.argv[2].split(',')
if len(category) > 0 and category[0] == '':
    category = database.db.cc2_image_mappings.distinct('categories.name', {'concept': concept})

product_ids = []
if len(sys.argv) > 3:
    product_list = sys.argv[3]

    with open('storage/' + product_list, 'rb') as csvfile:
        reader = csv.reader(csvfile)
        reader.next()
        for row in reader:
            product_ids.append(row[0])

# import pdb;pdb.set_trace()

condition_array = [
    {'concept': concept},
    {'categories.name': {'$in': category}},
    {'download_status': 'processed'},
    {'upload_status': 'pending'}
]

if len(product_ids) > 0:
    condition_array = [
        {'concept': concept},
        {'categories.name': {'$in': category}},
        {'download_status': 'processed'},
        {'upload_status': 'pending'},
        {'base_product_id': {'$in': product_ids}}
    ]


def upsert_tracker_document(product, status):
    tracker_doc = product
    if 'error' in tracker_doc and len(tracker_doc['error']) > 0:
        tracker_doc['upload_status'] = 'failure'
    else:
        tracker_doc['upload_status'] = status

    tracker_doc['updated_at'] = datetime.datetime.now()
    return database.db.cc2_image_mappings.find_and_modify({'base_product_id': product['base_product_id']},
                                                          {"$set": tracker_doc})


products = database.db.cc2_image_mappings.find(
    {
        '$and': condition_array
    })

if products.count() > 0:
    try:
        for product in products:
            # mark upload status as processing
            upsert_tracker_document(product, 'processing')
            if product['product_type'] != 'SINGLE-SKU':
                for color_key, color_value in enumerate(product['colors']):
                    if color_value['post_qc_desk_status'] == 'done' or color_value[
                        'post_qc_desk_status'] == 'finished':
                        cloud_images = []
                        for image_key, image_value in enumerate(color_value['retouched_images']):
                            cloud_image_flag = False
                            image_name = image_value['url'].split('//')[-1].split('studioimages')[-1]
                            delta = image_value['delta']
                            image_delta = utility_obj.delta_formation(delta)
                            formatted_image_name = color_value['color_variant_id'] + "-" + product[
                                'base_product_id'] + "_" + image_delta + '-2100' + '.jpg'
                            if path.exists(constant.SAN_LOCATION + '/' + image_name):

                                # upload image to cloud
                                image_path = constant.SAN_LOCATION + '/' + image_name
                                azureService.image_upload(image_path, formatted_image_name, product['concept'])
                                cloud_image_flag = True
                            else:
                                for download_image_key, download_image_value in enumerate(
                                        color_value['download_images']):
                                    if int(download_image_value['delta']) == int(delta):
                                        image_delta = utility_obj.delta_formation(download_image_value['delta'])
                                        image_path = constant.DOWNLOAD_LOCATION + '/' + download_image_value['rel_uri']
                                        azureService.image_upload(image_path, formatted_image_name, product['concept'])
                                        cloud_image_flag = True

                            if cloud_image_flag:
                                cloud_image = {}
                                cloud_image['delta'] = delta
                                cloud_image['style'] = '2100WX2100H'
                                cloud_image['url'] = formatted_image_name
                                cloud_images.append(cloud_image)
                        if len(cloud_images) > 0:
                            images = dict()
                            images['images'] = cloud_images
                            product['colors'][color_key]['cloud_images'] = images
            else:
                cloud_images = []
                if product['post_qc_desk_status'] == 'done' or product['post_qc_desk_status'] == 'finished':
                    for image_key, image_value in enumerate(product['retouched_images']):
                        cloud_image_flag = False
                        delta = image_value['delta']
                        image_name = image_value['url'].split('//')[-1].split('studioimages')[-1]
                        image_delta = utility_obj.delta_formation(image_value['delta'])
                        formatted_image_name = product[
                                                   'base_product_id'] + "_" + image_delta + '-2100' + '.jpg'
                        if path.exists(constant.SAN_LOCATION + '/' + image_name):
                            # upload image to cloud
                            image_path = constant.SAN_LOCATION + '/' + image_name
                            azureService.image_upload(image_path, formatted_image_name, product['concept'])
                            cloud_image_flag = True
                        else:
                            for download_image_key, download_image_value in enumerate(product['download_images']):
                                if int(download_image_value['delta']) == int(delta):
                                    image_path = constant.DOWNLOAD_LOCATION + '/' + download_image_value['rel_uri']
                                    azureService.image_upload(image_path, formatted_image_name, product['concept'])
                                    cloud_image_flag = True
                        if cloud_image_flag:
                            cloud_image = {}
                            cloud_image['delta'] = delta
                            cloud_image['style'] = '2100WX2100H'
                            cloud_image['url'] = formatted_image_name
                            cloud_images.append(cloud_image)
                    if len(cloud_images) > 0:
                        images = dict()
                        images['images'] = cloud_images
                        product['cloud_media'] = images

            upsert_tracker_document(product, 'processed')
    except Exception, e:
        error = dict()
        error[product['base_product_id']] = 'Some exception occur while running upload script!' + str(e)
        product['error'] = error
        upsert_tracker_document(product, 'failure')
